function searchMovie() {
    const movieTitle = document.getElementById('movieTitle').value;
    const apiKey = '30063268';
    const apiUrl = `https://www.omdbapi.com/?t=${movieTitle}&plot=full&apikey=${apiKey}`;

    fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
            if (data.Response === "True") {
                displayMovieInfo(data);
            } else {
                document.getElementById('movieInfo').innerHTML = "No se encontró ninguna película con ese título.";
            }
        })
        .catch(error => console.error('Hubo un error al obtener la información de la película:', error));
}

function displayMovieInfo(movie) {
    const movieInfoContainer = document.getElementById('movieInfo');
    const res = document.getElementById('lblResultado');
    res.innerHTML = `Resultados`;
    movieInfoContainer.innerHTML = `
        <p id="lblTitulo"><strong>Nombre: </strong>${movie.Title}</p>
        <p id="lblAño"><strong>Año de realización: </strong>${movie.Year}</p>
        <img src="${movie.Poster}" alt="Poster de la película">  
        <p><strong>Reseña:</strong> </p>
        <div id="lblReseña">${movie.Plot}</div> 
        <p id="lblActores"><strong>Actores principales:</strong> ${movie.Actors}</p>
    `;
}